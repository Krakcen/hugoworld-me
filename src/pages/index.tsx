import React, { ReactElement, useState, useRef, Suspense } from 'react'
import { Canvas, useFrame } from '@react-three/fiber'
import { OrbitControls, useGLTF } from '@react-three/drei'
import * as THREE from 'three'

// const Box = (props: JSX.IntrinsicElements['mesh']): JSX.Element => {
//   const mesh = useRef<THREE.Mesh>(null!)
//   // Set up state for the hovered and active state
//   const [hovered, setHover] = useState(false)
//   const [active, setActive] = useState(false)
//   // Subscribe this component to the render-loop, rotate the mesh every frame
//   useFrame(() => (mesh.current.rotation.x += 0.01))

//   // Return the view, these are regular Threejs elements expressed in JSX
//   return (
//     <mesh
//       {...props}
//       ref={mesh}
//       scale={active ? 2 : 1}
//       onClick={() => setActive(!active)}
//       onPointerOver={() => setHover(true)}
//       onPointerOut={() => setHover(false)}
//     >
//       <boxGeometry args={[1, 1, 1]} />
//       <meshStandardMaterial color={hovered ? 'limegreen' : 'orange'} />
//     </mesh>
//   )
// }

const SkateBoard = (): JSX.Element | null => {
  const { nodes } = useGLTF('/3d/bread/scene.gltf')
  const { Object_2 } = nodes

  if (Object_2 instanceof THREE.Mesh) {
    return (
      <group>
        <scene name="Scene" position={[0, 0, 0]}>
          <mesh
            name="Object_2"
            morphTargetDictionary={Object_2.morphTargetDictionary}
            morphTargetInfluences={Object_2.morphTargetInfluences}
            rotation={[1.5707964611537577, 0, 0]}
          >
            <bufferGeometry attach="geometry" {...Object_2.geometry} />
            <meshStandardMaterial
              attach="material"
              {...Object_2.material}
              name="Material_0_COLOR_0"
            />
          </mesh>
        </scene>
      </group>
    )
  }
  return null
}

const Index = (): ReactElement => {
  return (
    <div className="fullSize">
      <Canvas camera={{ position: [0, 0, 35] }}>
        <ambientLight intensity={2} />
        {/* <pointLight position={[-40, -40, -40]} /> */}

        <OrbitControls />
        <Suspense fallback={null}>
          <SkateBoard />
        </Suspense>
      </Canvas>
    </div>
  )
}

export default Index
