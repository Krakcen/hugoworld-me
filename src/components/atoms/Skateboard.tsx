import React, { Suspense, useRef, useEffect } from 'react'
import { Canvas } from '@react-three/fiber'
import { OrbitControls, useGLTF, useAnimations } from '@react-three/drei'
import * as THREE from 'three'

const Model = (): JSX.Element | null => {
  const skate = useRef(useGLTF('/3d/skateboard/scene.gltf'))
  const { ref } = useAnimations(skate.current.animations)

  useEffect(() => {
    if (ref.current instanceof THREE.Object3D) {
      ref.current.rotation.z += 20
      ref.current.rotation.y += 20
    }
  }, [])

  return <primitive ref={ref} object={skate.current.scene} position={[0, 0, 0]} />
}

const SkateBoard = (): JSX.Element | null => {
  return (
    <Canvas gl={{ antialias: true }} camera={{ position: [8, 0, 0] }}>
      <directionalLight position={[0, 10, 0]} />
      <directionalLight position={[0, -10, 0]} />
      <OrbitControls autoRotate autoRotateSpeed={6} enablePan={false} enableZoom={false} />
      <Suspense fallback={null}>
        <Model />
      </Suspense>
    </Canvas>
  )
}

export default SkateBoard
