import { strapiLocales } from '../constants'
import { StrapiLocales } from '../types/global'

export const getStrapiURL = (path = ''): string => {
  if (process.env.NEXT_PUBLIC_STRAPI_API_URL)
    return `https://${process.env.NEXT_PUBLIC_STRAPI_API_URL}${path}`
  return `http://localhost:1337${path}`
}

export const fetchAPI = async <T>(path: string): Promise<T> => {
  const requestUrl = getStrapiURL(path)
  const response = await fetch(requestUrl)
  const data = await response.json()
  return data
}

export const getStrapiLocale = (locale: StrapiLocales): string => strapiLocales[locale]
